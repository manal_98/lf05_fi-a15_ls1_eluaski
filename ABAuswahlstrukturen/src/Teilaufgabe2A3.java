import java.util.Scanner;

public class Teilaufgabe2A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine Zahl ein: " );
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine dritte Zahl ein: ");
		
		int zahl3 = myScanner.nextInt();
		
		if (zahl3 > zahl2 && zahl3 > zahl1)
		{
			System.out.println(zahl3 + " ist die größte Zahl.");
		}
		else 
		{
			if (zahl1 > zahl2 && zahl1 > zahl3)
			{
				System.out.println(zahl1 + " ist die größte Zahl.");
			}
			else 
			{
				System.out.println(zahl2 + " ist die größte Zahl.");
			}
			
		}
	
		myScanner.close(); 
	}

}
