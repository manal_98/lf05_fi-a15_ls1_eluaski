import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 1.	a) Wenn ich schlafen gehe, dann putz ich mir die Zähne.
		//		b) Wenn ich koche, dann bekomme ich ein Gericht.
		//		c) Wenn ich lerne, dann kann ich Aufgaben einfacher lösen. 

		// 2.	Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (if)
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:"); 
		
		int zahl1 = myScanner.nextInt();	
		
		System.out.println("Geben Sie eine zweite Zahl ein:");
			
		int zahl2 = myScanner.nextInt(); 
		
		if (zahl1 == zahl2)
		{	
			System.out.println("Die beiden Zahlen sind gleich!");
		}
	
		// 3. Wenn die 2. Zahl größer als die 1. Zahl ist, soll eine Meldung ausgegeben werden (if)
		
		if (zahl2 > zahl1)
		{
			System.out.println(zahl2 + " ist größer als " + zahl1 + ".");
		}
		
		 myScanner.close(); 
		 
	}

}
