import java.util.Scanner;

public class Teilaufgabe2A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 1. Wenn die 1. Zahl größer als die 2. Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben werden (If mit && / Und)

		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine Zahl ein: " );
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine dritte Zahl ein: ");
		
		int zahl3 = myScanner.nextInt();
		
		if (zahl1 > zahl2 && zahl1 > zahl3)
		{
			System.out.println(zahl1 + " ist größer als " + zahl2 + " und " +zahl3 + ".");
		}
		
		
		myScanner.close(); 
	}

}
