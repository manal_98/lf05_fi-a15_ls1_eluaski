import java.util.Scanner;

public class Aufgabe5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe 5 BMI
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie Ihr Geschlecht an (m/w): ");
		
		String geschlecht = scan.next();
		
		System.out.print("Bitte geben Sie Ihr Gewicht an (kg): ");
		
		float gewicht = scan.nextFloat();
		
		System.out.print("Bitte geben Sie Ihre Größe an(cm): ");
		
		float groesse = scan.nextFloat();
		float bmi = (gewicht / ((groesse / 100) * (groesse / 100))); 
			
		if (geschlecht.contentEquals("m") && bmi < 20) 
		{
			System.out.printf("Sie haben einen BMI von %.0f" + " und sind somit in der Untergewicht-Klasse.", bmi);
		}
		else if (geschlecht.contentEquals("m") && bmi >= 20 && bmi <= 25) 
		{
			System.out.printf("Sie haben einen BMI von %.0f" + " und sind somit in der Normalgewicht-Klasse.", bmi);
		}
		else if (geschlecht.contentEquals("m") && bmi > 25) 
		{
			System.out.printf("Sie haben einen BMI von %.0f" + " und sind somit in der Übergewicht-Klasse.", bmi);
		}
		else if (geschlecht.contentEquals("w") && bmi < 19) 
		{
			System.out.printf("Sie haben einen BMI von %.0f" + " und sind somit in der Untergewicht-Klasse.", bmi);
		}
		else if (geschlecht.contentEquals("w") && bmi >= 19 && bmi <= 24) 
		{
			System.out.printf("Sie haben einen BMI von %.0f" + " und sind somit in der Normalgewicht-Klasse.", bmi);
		}
		else
		{
			System.out.printf("Sie haben einen BMI von %.0f" + " und sind somit in der Übergewicht-Klasse.", bmi);
		}
		
		scan.close();
	}

}
