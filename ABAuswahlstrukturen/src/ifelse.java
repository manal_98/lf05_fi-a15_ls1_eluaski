import java.util.Scanner;

public class ifelse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:"); 
		
		int zahl1 = myScanner.nextInt();	
		
		System.out.println("Geben Sie eine zweite Zahl ein:");
			
		int zahl2 = myScanner.nextInt(); 
		
	// 4. Wenn die 1.Zahl größer oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben werden, ansonsten eine andere Meldung (If-Else)
		
		if (zahl1 >= zahl2)
		{
			System.out.println(zahl1 + " ist größer/gleich " + zahl2 + ".");
		}
		else 
		{
			System.out.println(zahl1 + " ist nicht größer als " + zahl2 + ".");
		}
		
		 myScanner.close(); 
		
	}

}
