import java.util.Scanner;

public class Teilaufgabe1A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 2. Wenn die 3. Zahl größer als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung ausgegeben werden (if mit || / Oder)
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine Zahl ein: " );
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine dritte Zahl ein: ");
		
		int zahl3 = myScanner.nextInt();
		
		if (zahl3 > zahl2 || zahl3 > zahl1)
		{
			if (zahl3 > zahl2)
			{
				System.out.println(zahl3 + " ist größer als " + zahl2);
			}
			else
			{
				System.out.println(zahl3 + " ist größer als " + zahl1);
			}
		}
		
	
		myScanner.close(); 
	}

}
