import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Geben Sie Ihr Nettogehalt an: ");
		
		float netto = myScanner.nextFloat();
		
		System.out.print("Ermäßigter Steuersatz? Geben Sie j für JA oder n für NEIN ein: ");
		
		String steuersatz = myScanner.next();
		
		if (steuersatz.equals("j"))
		{
			System.out.print(netto-(netto*0.07));
		}
		else if (steuersatz.equals("n"))
		{
			System.out.print(netto-(netto*0.19));
		}
		else
		{
			System.out.print("Falsche Eingabe. Eingabe muss j oder n sein.");
		}
		
		myScanner.close();
		
		
	}

}
