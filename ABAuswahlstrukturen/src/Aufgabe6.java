import java.util.Scanner;
import java.lang.Math; 


public class Aufgabe6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe 6 Funktionslöser

		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bitte geben sie einen Wert für x an: ");
		
		double x = scan.nextDouble();
		double e = 2.718;
		
		if (x <= 0 )
		{
			System.out.printf("y = %.2f" + " und liegt somit im exponentiellen Bereich.", Math.pow(e, x));
		}
		else if (x > 0 && x <= 3)
		{
			System.out.printf("y = %.2f" + " und liegt somit im quadratischen Bereich.", Math.pow(x, 2) + 4);
		}
		else
		{
			System.out.printf("y = %.2f" + " und liegt somit im linearen Bereich.", 2 * x + 4);
		}
		
		scan.close();
		
	}

}
