import java.util.Scanner;
public class Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Aufgabe 4 Rabattsystem
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bestellwert: ");
		
		double bestellwert = scan.nextDouble();
		
		if (bestellwert <= 100)
		{
			System.out.printf("%.2f €", (bestellwert - ((bestellwert + (bestellwert * 0.19)) * 0.1)));
		}
		else if (bestellwert > 100 && bestellwert <= 500)
		{
			System.out.printf("%.2f €", (bestellwert - ((bestellwert + (bestellwert * 0.19)) * 0.15)));
		}
		else
		{
				System.out.printf("%.2f €", (bestellwert - ((bestellwert + (bestellwert * 0.19)) * 0.2)));
		}
		
		scan.close();
	}

}
