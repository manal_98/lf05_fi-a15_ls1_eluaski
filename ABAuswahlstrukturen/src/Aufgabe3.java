import java.util.Scanner;
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Aufgabe 3: Hardware-Großhändler
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Einzelpreis Maus: ");
		
		float einzelpreis = scan.nextFloat();
		
		System.out.print("Anzahl Mäuse: ");
		
		int anzahl = scan.nextInt();
		
		if (anzahl >= 10)
		{
			System.out.printf("%.2f €",((einzelpreis + (einzelpreis * 0.19)) * anzahl));
		}
		else 
		{
			System.out.printf("%.2f €",((einzelpreis + (einzelpreis * 0.19)) * anzahl) + 10);
		}
	
		scan.close();
	}

}
