
public class Aufgabe1 {

	public static void main(String[] args) {
		
		String s = "**";
		
		System.out.printf( "%20s\n", s );
		System.out.printf( "%16.1s", s );
		System.out.printf( "%7.1s\n", s );
		System.out.printf( "%16.1s", s );
		System.out.printf( "%7.1s\n", s );
		System.out.printf( "%20s\n", s );
	}

}
