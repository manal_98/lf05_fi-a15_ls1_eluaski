
public class VariablenVergeben {

	public static void main(String[] args) {
		
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		//anzahlDurchlaeufe

		/* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		
		byte anzahlDurchlaeufe = 25;
		
		System.out.println("Durchlaeufe: " + anzahlDurchlaeufe);

		/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		//menue

		/* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		
		char menue = 'C';
		
		System.out.println(menue);

		/* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		//lichtgeschwindigkeit

		/* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		
		long lichtgeschwindigkeit = 299792458l;
		
		System.out.println("Lichtgeschwindigkeit in ms: " + lichtgeschwindigkeit);

		/* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		//mitgliederVerein

		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		
		byte mitgliederVerein = 7;
		
		System.out.println("Mitglieder im Verein: " + mitgliederVerein);

		/* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		//elektrElementarladung 
		
		double elektrElementarladung = -1.602 * (10^-19);
		
		System.out.println("Elektrische Elementarladung: " + elektrElementarladung);
		
		
		 /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		//zahlung

		/*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		 
		 boolean zahlung = true;
		 
		 System.out.println("Zahlung erfolgt: " + zahlung);


	}

}
