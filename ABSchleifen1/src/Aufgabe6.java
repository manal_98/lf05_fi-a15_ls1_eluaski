import java.util.Scanner;
public class Aufgabe6 {

	public static void main(String[] args) {
		// Aufgabe 6 Sterne
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Geben Sie bitte eine Anzahl an Sternchen an: ");
		
		int anzahl = scan.nextInt();
		
		scan.close();
		
		for (int i = 0; i < anzahl; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(" * ");
			}
			
			System.out.print(" \n");
			
		}

	}

}
