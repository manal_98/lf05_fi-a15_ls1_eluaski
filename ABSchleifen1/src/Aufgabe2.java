import java.util.Scanner;
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe 2 Summe
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte einen begrenzenden Wert für n ein: ");
		
		int n = scan.nextInt();
		
		scan.close();
		
		int sum1 = 0;
		int sum2 = 0;
		int sum3 = 0;
		
		for (int i = 1; i <= n; i++)
		{
			sum1 += i;
		}
		System.out.println("Die Summe für A beträgt: " + sum1);
	
		for (int i = 2; i <= 2 * n; i+=2)
		{
			sum2 += i;
		}
		System.out.println("Die Summe für B beträgt: " + sum2);
		
		for (int i = 1; i <= 2 *n +1; i+=2)
		{
			sum3 += i;
		}
		System.out.println("Die Summe für C beträgt: " + sum3);

	}

}
