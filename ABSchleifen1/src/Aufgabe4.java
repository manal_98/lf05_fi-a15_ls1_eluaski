
public class Aufgabe4 {

	public static void main(String[] args) {
		
		System.out.print("a) ");
		
		for (int i = 100; i >= 9; i--) {
			if (i % 3 == 0)
			{
				System.out.print(i + ", ");
			}
		}
		
		System.out.print("\n");
		System.out.print("b) ");
		for (int a = 1; a <= 20; a++) {
			
			System.out.print(a * a + " ,");
		}
		
		
		System.out.print("\n");
		System.out.print("c) ");
	
		for (int j = 2; j <= 102; j+=4) {
			
				System.out.print(j + " ,");
		}
		
		System.out.print("\n");
		System.out.print("d) ");
		
		for (int k = 2; k <= 32; k+=2) {
			
			System.out.print(k * k + " ,");
			
		}
		
		System.out.print("\n");
		System.out.print("e) ");
		
		for (int l = 1; l <= 15; l++) {
			
			System.out.printf("%.0f, ", Math.pow(2, l));
			
		}
	}

}
