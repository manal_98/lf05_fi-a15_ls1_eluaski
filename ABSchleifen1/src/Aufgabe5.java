
public class Aufgabe5 {

	public static void main(String[] args) {
		
		 for( int z = 1; z <= 10; z++) //Zeilen
		    {
		
		      for( int s = 1; s <= 10; s++) //Spalten
		      {
		         //Abstand                                       
		        if( z * s < 10) 
		        	{
		        	System.out.print(" ");
		        	}
		        if( z * s < 100) 
		        	{
		        	System.out.print(" ");
		        	}

		        System.out.print( "  " + z * s); // Inhalt bzw Produkt
		      }

		      System.out.println();   //Zeilenumbruch pro Zeile         
		    }

	}
}