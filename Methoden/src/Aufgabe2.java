public class Aufgabe2 {

	public static void main(String[] args) {

		System.out.println("Dieses Programm zeigt die Multiplikation von x und y.");
		System.out.println("-------------------------------------------------------");
		
		double x = 2.36;
		double y = 7.87;
		
		double ergebnis = multiplikation(x, y);
		
		System.out.printf("x = %.2f , y = %.2f und x * y = %.2f\n", x, y, ergebnis);
	}
	
	public static double multiplikation(double wert1, double wert2) {
		
		return wert1 * wert2;
				
	}

}
