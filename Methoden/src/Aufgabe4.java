import java.math.*;

public class Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//a) Würfel
		double a = 2.56;
		double ergebnisw = wuerfel(a);
		
		System.out.println("Berechnung Volumen eines Würfels:");
		System.out.printf("a = %.2f und V = %.2f\n" , a, ergebnisw);
		
		double b = 3.23;
		double c = 8.32;
		double ergebnisq = quader(a, b, c);
		
		System.out.println("Berechnung eines Quaders:");
		System.out.printf("a = %.2f, b = %.2f, c = %.2f und V = %.2f\n", a, b, c, ergebnisq);

		double h = 7.34;
		double ergebnisp = pyramide(a, h);
		
		System.out.println("Berechnung einer Pyramide:");
		System.out.printf("a = %.2f, h = %.2f und V = %.2f\n", a, h, ergebnisp);
		
		double r = 3.54;
		double ergebnisk = kugel(r);
		
		System.out.println("Berechnung einer Kugel:");
		System.out.printf("r = %.2f und V = %.2f\n", r, ergebnisk);
	}
	
	public static double wuerfel (double wert1) {
		return wert1 * wert1 * wert1;
	}
	
	public static double quader (double wert1, double wert2, double wert3) {
		return wert1 * wert2 * wert3;
	}
	public static double pyramide (double wert1, double wert2) {
		return wert1 * wert1 * wert2/3.0;
	}
	public static double kugel (double wert1) {
		return 4.0/3.0 * wert1 * wert1 * wert1 * Math.PI;
	}
}