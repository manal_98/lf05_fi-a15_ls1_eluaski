﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args) {
    	do {
    		double zwischenspeicher;
    		
    		zwischenspeicher = fahrkartenbestellungErfassen();
    	
    		zwischenspeicher = fahrkartenBezahlen(zwischenspeicher);
    	
    		fahrkartenAusgeben();
    	
    		rueckgeldAusgeben(zwischenspeicher);
    	}while(true);
    }
    
    public static double fahrkartenbestellungErfassen() {
    	
        int		anzahlTickets;
        double ticketpreis;
        int ticket;
        
        //Auswahl beschreiben
        System.out.println("\n");
        System.out.println("Fahrkartenbestellvorgang:");
        System.out.println("===========================\n");
        System.out.println("Waehlen Sie ihre Wunschfahrkarte für Berlin AB aus:");

        System.out.println("   Einzelfahrschein Regeltarif AB [3,00 EUR] (1)");
        System.out.println("   Tageskarte Regeltarif AB [8,80 EUR] (2)");
        System.out.println("   Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)");
       
        do{
        	System.out.println("Bitte geben Sie Ihre Wahl an:");
            ticket = tastatur.nextInt();
        	
            switch(ticket) {
        		case 1:
        			ticketpreis = 3.00;
        			break;
        		case 2:
        			ticketpreis = 8.80;
        			break;
        		case 3:
        			ticketpreis = 25.50;
        			break;
        		default:
        			System.out.println("Ungültige Eingabe!");
        			ticketpreis = 0.00;
        	}
        }while(ticket < 1 || ticket > 3);
        
        
        // Preis pro Ticket statt Gesamtpreis
        //System.out.print("Preis pro Ticket (EURO): ");
        //zuZahlenderBetrag = tastatur.nextDouble();
        
        // Nachfrage zu Eingabe der Zahl der gewünschten Tickets
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        
        // Check ob eine Zahl von mindestens einem Ticket eingegeben wurde.
        // 0 und negative Zahlen sind ungültig und fragen nach erneuter Eingabe
        while (anzahlTickets < 1) {
     	   System.out.println("Ungültige Zahl an Tickets");
     	   System.out.print("Bitte geben Sie eine Zahl größer als 0 ein: ");
     	   anzahlTickets = tastatur.nextInt();
        }
        if (anzahlTickets >= 1 && anzahlTickets <= 10) 
        {
        // Der zu Zahlende Betrag wird mit der Zahl der Tickets multipliziert 
        	return ticketpreis * (double) anzahlTickets; 
        }
        else 
        {
        	// Wenn die Anzahl der Tickets über 10 hinaus geht, wird mit dem Standardwert 1 weitergemacht
        	System.out.println("Ungültige Zahl an Tickets(Maximum 10). Es wird nur 1 Ticket berechnet.");
        	return ticketpreis * (double) 1; 
        }
      
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	
    	// Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
        return eingezahlterGesamtbetrag - zuZahlenderBetrag; 

    }
    
    public static void fahrkartenAusgeben() {

    	// Fahrscheinausgabe
	    // -----------------
	    // Wenn mehr als ein Fahrschein gekauft wurde ändert sich der Text
	 	System.out.println("\n Ihre Fahrscheine werden ausgegeben");
	
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {

        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht Ihre Fahrscheine\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
    
}