
public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int alter = 22;
		String name = "Manal";
		
		System.out.println("Ich heiße "+ name +".\n"
				+ "Ich bin "+ alter +" Jahre alt.\n"
				+ "Dieser Satz beinhaltet ein \"Gänsefüßchen\".");
		//ln steht für line also Zeile. Der einzige Unterschied zwischen print() und println() ist das Letzteres noch einen Zeilenumbruch/Absatz macht.

	}

}
