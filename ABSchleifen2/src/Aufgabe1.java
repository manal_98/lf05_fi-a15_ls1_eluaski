import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Geben Sie einen Wert an: ");
		int n = scan.nextInt();
		
		scan.close();
		
		int i = 1;
		
		while (n >= i) 
		{
			System.out.println(i);
			i++;
		}
	}

}
