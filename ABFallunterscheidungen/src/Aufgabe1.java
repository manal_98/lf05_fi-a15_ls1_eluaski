import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// AUfgabe 1 Noten
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl zwischen 1-6 an: ");
		
		int x = scan.nextInt();
		
		switch (x) {
		case 1:
			
			System.out.print("Sehr gut");
			
			break;
		case 2:
		
			System.out.print("Gut");
			
			break;
		case 3:
	
			System.out.print("Befriedigend");
	
			break;
		case 4:
	
			System.out.print("Ausreichend");
	
			break;
		case 5:
	
			System.out.print("Mangelhaft");
		
			break;
		case 6:
			
			System.out.print("UNgenügend");
			
			break;
		default:
			
			System.out.print("Bitte geben Sie NUR ganze Zahlen von 1-6 an!");
			break;
		}
		
		scan.close();
	}

}

