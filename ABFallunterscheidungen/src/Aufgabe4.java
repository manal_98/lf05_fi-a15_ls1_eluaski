import java.util.Scanner;

public class Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe 4 Taschenrechner
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine Zahl für x ein: ");
		
		double x = scan.nextDouble();
		
		System.out.print("Bitte geben Sie eine Zahl für y ein: ");
		
		double y = scan.nextDouble();
		
		System.out.print("Für Addition geben Sie +, für Subtraktion -, für Multiplikation * und für Division / ein: " );
		
		String rop = scan.next();
		
		switch (rop) {
		case "+":
			
			System.out.printf("%.2f", x + y);
			
			break;
		case "-":
			
			System.out.printf("%.2f", x - y);
			break;
		case "*":
			
			System.out.printf("%.2f", x * y);
			
			break;
		case "/":
			
			System.out.printf("%.2f", x / y);
			
			break;
		default:
			
			System.out.print("Fehler! Erlaubte Zeichen: + - * /");
			break;
		}
		scan.close();
	}

}
