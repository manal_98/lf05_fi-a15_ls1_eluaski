import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Aufgabe 3 Römische Zahlen
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ein römisches Zahlzeichen an, um die entsprechende Dezimalzahl zu ermitteln: ");
		
		String romzahl = scan.next();
		
		switch (romzahl) {
		case "I":
			
			System.out.print("1");
			
			break;
		case "V":
			
			System.out.print("5");
			
			break;
		case "X":
			
			System.out.print("10");
			
			break;
		case "L":
			
			System.out.print("50");
			
			break;
		case "C":
			
			System.out.print("100");
			
			break;
		case "D":
			
			System.out.print("500");
			
			break;
		case "M":
			
			System.out.print("1000");
			
			break;
		default:
			
			System.out.print("Dies ist kein römisches Zahlenzeichen! Versuchen Sie es erneut.");
			
			break;
		}
		scan.close();
	}

}
