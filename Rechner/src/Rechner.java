import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: " );
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		int ergebnis = zahl1 + zahl2;
		
		System.out.print("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
		
		System.out.print("\n\nBitte geben Sie eine ganze Zahl ein: " );
		
		int zahl3 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl4 = myScanner.nextInt();
		
		int multi = zahl3 * zahl4;
		
		System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
	    System.out.print(zahl3 + " * " + zahl4 + " = " + multi);   
	    
	    System.out.print("\n\nBitte geben Sie eine ganze Zahl ein: " );
		
		int zahl5 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl6 = myScanner.nextInt();
		
		int subtraktion = zahl5 - zahl6;
		
		System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
	    System.out.print(zahl5 + " - " + zahl6 + " = " + subtraktion); 
	    
	    System.out.print("\n\nBitte geben Sie eine ganze Zahl ein: " );
		
		int zahl7 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl8 = myScanner.nextInt();
		
		int divid = zahl7 / zahl8;
		
		System.out.print("\n\n\nErgebnis der Division lautet: "); 
	    System.out.print(zahl7 + " : " + zahl8 + " = " + divid);
	 
	    myScanner.close(); 
	}

}
